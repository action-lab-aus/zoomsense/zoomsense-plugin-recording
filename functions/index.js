const functions = require('firebase-functions');
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const admin = require('firebase-admin');
admin.initializeApp();
const db = admin.database();
const axios = require('axios');
const FUNCTIONS_URL = environment.functions_url;

/**
 * When `actualStartTime` is added to the meeting node, push the data to the /data/meetingMeta
 * node for triggering the recording plugin
 *
 * @param {String} uid Host ID
 * @param {String} meetingId Session ID
 */
exports.onMeetingStartedAdded = functions.database
  .ref('/meetings/{uid}/{meetingId}/actualStartTime')
  .onCreate(async (snapshot, context) => {
    try {
      const actualStartTime = snapshot.val();
      const { uid, meetingId } = context.params;
      const patientUid = (await db.ref(`meetings/${uid}/${meetingId}`).once('value')).val().patientUid;

      await db.ref(`/data/meetingMeta/${meetingId}`).set({ uid, actualStartTime, patientUid });
    } catch (error) {
      functions.logger.error('onMeetingStartedAdded Error: ', error);
    }
  });

/**
 * When `actualStartTime` is added to /data/meetingMeta, fetch the plugins for the current
 * section from /config and push the `recordingStart` flag if recording is enabled
 *
 * @param {String} meetingId Session ID
 */
exports.onActualStartTimeAdded = functions.database
  .ref('/data/meetingMeta/{meetingId}/actualStartTime')
  .onCreate(async (snapshot, context) => {
    try {
      const meetingId = context.params.meetingId;

      // Fetch the current plugins object
      const plugins = (await db.ref(`/config/${meetingId}/current/currentState/plugins`).once('value')).val();
      if (plugins['recording'] && plugins['recording'].enabled) {
        // If the recording plugin is enabled for the current session, push the recording start flag
        await db.ref(`/data/recording/${meetingId}/recordingStart`).set(true);
      }
    } catch (error) {
      functions.logger.error('onActualStartTimeAdded Error: ', error);
    }
  });

/**
 * When `recordingStart` flag is added to /data/recording, call the OpenTok firebase function
 * endpoint to start archiving if the current meeting is a Vonage meeting
 *
 * @param {String} meetingId Session ID
 */
exports.onRecordingStartFlagAdded = functions.database
  .ref('/data/recording/{meetingId}/recordingStart')
  .onCreate(async (snapshot, context) => {
    try {
      const meetingId = context.params.meetingId;

      // Check whether the meeting is a Vonage meeting
      const meta = (await db.ref(`/data/meetingMeta/${meetingId}`).once('value')).val();
      const meeting = (await db.ref(`meetings/${meta.uid}/${meetingId}`).once('value')).val();

      // If the meeting is a Vonage meeting, call the opentok endpoint to start archive
      if (meeting.type === 'Vonage')
        await axios.post(`${FUNCTIONS_URL}opentokApp-opentokApp/archive/start/${meeting.session}`, {
          meetingId,
        });
    } catch (error) {
      functions.logger.error('onRecordingStartFlagAdded Error: ', error);
    }
  });

/**
 * When `recordingStart` flag is added to /data/recording, call the OpenTok firebase function
 * endpoint to start archiving if the current meeting is a Vonage meeting
 *
 * @bodyParam id Vonage archive ID
 * @bodyParam sessionId Vonage Session ID
 * @bodyParam status Vonage Archive Status (e.g. `started`, `paused`, `stopped`, `uploaded`, `failed`)
 */
exports.archiveOnUploaded = functions.https.onRequest(async (req, res) => {
  try {
    // Parse the request payload and extract the notification topic
    const { id, sessionId, status } = req.body;

    if (status === 'uploaded') {
      const sessionSnap = await db.ref(`sessionRef/${sessionId}`).once('value');
      const { coachUid, meetingId } = sessionSnap.val();

      // Set the curationAssets data node with the archive uploaded status
      await db.ref(`data/recording/${meetingId}`).update({
        status: 'uploaded',
        s3UrlVideo: `${environment.opentok.api_key}/${id}/archive.mp4`,
      });

      // Store extra reference node from archive id to coach uid & meeting id.
      // When transcription is completed, the only information we can get is the
      // archive id. Use the archive reference node to find the curation data node
      // to be updated
      await db.ref(`archiveRef`).child(id).set({
        coachUid,
        meetingId,
      });
    }

    res.status(200).send('Recording file uploaded to S3');
  } catch (error) {
    functions.logger.error(error);
    res.status(500).send(error);
  }
});
